import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';


@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
   styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class UsersComponent implements OnInit {

  users;
  isLoading=true;
  currentUser;

  select(user){
		this.currentUser = user; 
    console.log(	this.currentUser);
 }

 addUser(user){
   this._userService.addUser(user);
 }

updateUser(user){
this._userService.updateUser(user);
}

deleteUser(user){
  //this.users.splice(this.users.indexOf(user),1)
  this._userService.deleteUser(user);
}


constructor(private _userService:UsersService) { }

  ngOnInit() {

    this._userService.getUsrsFromApi().subscribe(usersData =>{this.users=usersData.result; this.isLoading=false;
 //   this._userService.getUsers().subscribe(usersData =>{this.users=usersData; this.isLoading=false;
    console.log(this.users)});


  }

}
