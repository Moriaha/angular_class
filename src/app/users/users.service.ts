import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
// private _url ='http://jsonplaceholder.typicode.com/users';

//conection to the API I write 
private _url ='http://moriaha.myweb.jce.ac.il/api/users';


usersObservable;

getUsrsFromApi(){
  return this._http.get(this._url).map(res => res.json());
}


//method to recieve users from URL - on comment
getUsers(){
//return this._http.get(this._url).map(res => res.json()).delay(2000);
this.usersObservable = this.af.database.list('/users');
return this.usersObservable;
}


updateUser(user){
  let userKey = user.$key;
  let userData = {name: user.name, email: user.email};
  this.af.database.object('/users/'+userKey).update(userData);
}

deleteUser(user){
  let userKey = user.$key;
  this.af.database.object('/users/'+userKey).remove();
}


addUser(user){
this.usersObservable.push(user);
}

  constructor(private af:AngularFire, private _http:Http) { }

}







//old method to get users hard coded
//getUsers(){
//let users = [
 // {name:'John', email:'john@gmail.com'},
 // {name:'Moria', email:'moria@gmail.com'},
 // {name:'Eli', email:'eli@gmail.com'},
//]
 //return users;
//}
