import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostsComponent } from './posts/posts.component';
import { UserFormComponent } from './user-form/user-form.component';


  export const firebaseConfig = {
    apiKey: "AIzaSyBNpHJbD4-sllxZhAQNvSk5s5e3n-HcRjk",
    authDomain: "angular-4d52c.firebaseapp.com",
    databaseURL: "https://angular-4d52c.firebaseio.com",
    storageBucket: "angular-4d52c.appspot.com",
    messagingSenderId: "480878009474"
  };

const appRoutes: Routes =[
	  {path:'users', component:UsersComponent},
	  {path:'posts', component:PostsComponent},
	  {path:'', component:UsersComponent},
	  {path:'**', component:PageNotFoundComponent}
	];



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostsComponent,
    UserFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
   RouterModule.forRoot(appRoutes),
   AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
